#ifndef SOMEFUNCS_H
#define SOMEFUNCS_H

using namespace std;

void split(const string &s, char delim, vector<string> &elems);
float s2f ( const string& s );
int s2i ( const string& s );
float VectorLength( const cFileOBJ::Vertex3d& v );


#endif // SOMEFUNCS_H