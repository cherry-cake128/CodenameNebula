class AiUWindowLayoutBase expands AiUWindowList;

enum EVerticalAlign
{
	VA_Middle,
	VA_Top,
	VA_Bottom,
};

enum EHorizAlign
{
	HA_Center,
	HA_Left,
	HA_Right,
};

defaultproperties
{
}
