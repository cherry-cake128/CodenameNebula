class AiUWindowLayoutCell expands AiUWindowLayoutBase;


// Cell Layout Parameters
var int					RowSpan;
var int					ColSpan;
var EHorizAlign			HAlign;
var EVerticalAlign		VAlign;

var AiUWindowDialogControl C;

defaultproperties
{
}
