//=============================================================================
// CrateExplosiveMed.
//=============================================================================
class CrateExplosiveMed extends Containers;

defaultproperties
{
     HitPoints=8
     bExplosive=True
     explosionDamage=450
     explosionRadius=1000.000000
     ItemName="Explosive Crate"
     bBlockSight=True
     Mesh=LodMesh'CNN.CrateExplosiveMed'
     CollisionRadius=21.600000
     CollisionHeight=24.000000
     Mass=40.000000
     Buoyancy=50.000000
}
