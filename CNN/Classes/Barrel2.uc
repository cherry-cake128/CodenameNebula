//=============================================================================
// Barrel2.
//=============================================================================
class Barrel2 extends Containers;

defaultproperties
{
     bInvincible=True
     bFlammable=False
     ItemName="Renforced Barrel"
     bBlockSight=True
     Mesh=LodMesh'CNN.Barrel2'
     CollisionRadius=21.000000
     CollisionHeight=29.000000
     Mass=80.000000
     Buoyancy=90.000000
}