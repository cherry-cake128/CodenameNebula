//=============================================================================
// Cannister.
//=============================================================================
class Cannister extends Containers;

defaultproperties
{
     HitPoints=10
     bInvincible=True
     bFlammable=False
     ItemName="Helium-3 Cannister"
     Mesh=LodMesh'CNN.Cannister'
     CollisionRadius=8.500000
     CollisionHeight=9.375000
     Mass=10.000000
     Buoyancy=10.000000
}