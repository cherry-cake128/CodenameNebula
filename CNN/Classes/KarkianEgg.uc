//=============================================================================
// KarkianEgg.
//=============================================================================
class KarkianEgg extends DeusExDecoration;

defaultproperties
{
     bInvincible=True
     bFlammable=False
     bPushable=False
     ItemName="Incubated Greasel Egg"
     Mesh=LodMesh'CNN.KarkianEgg'
     CollisionRadius=22.000000
     CollisionHeight=11.500000
     Mass=40.000000
     Buoyancy=40.000000
}