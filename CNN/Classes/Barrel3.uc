//=============================================================================
// Barrel3.
//=============================================================================
class Barrel3 extends Containers;

defaultproperties
{
     bInvincible=True
     bFlammable=False
     ItemName="Radioactive Barrel"
     bBlockSight=True
     Mesh=LodMesh'CNN.Barrel3'
     CollisionRadius=19.000000
     CollisionHeight=30.000000
     AmbientSound=Sound'Ambient.GeigerLoop'
     SoundRadius=6
     SoundVolume=32
     Mass=80.000000
     Buoyancy=90.000000
}