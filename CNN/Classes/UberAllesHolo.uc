//=============================================================================
// UberAlles.
//=============================================================================
class UberAllesHolo extends UberAlles;

defaultproperties
{
	Orders=Standing
    bShowPain=False
    bHateShot=False
    bHateInjury=False
    bReactPresence=False
    bReactAlarm=False
    bReactProjectiles=False
    Tag=UberAlles
    bUnlit=True
    bCollideActors=False
    bBlockActors=False
    bBlockPlayers=False

}
