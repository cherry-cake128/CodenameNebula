class NYLibertyTopEvidence extends CarWrecked;

defaultproperties
{
	bCanBeBase=True
	bInvincible=False
	Physics=PHYS_None
	Mesh=LodMesh'DeusExDeco.NYLibertyTop'
	CollisionRadius=500.000000
	CollisionHeight=390.000000
	Mass=10000.000000
	Buoyancy=1000.000000
}
