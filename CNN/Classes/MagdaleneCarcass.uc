//=============================================================================
// MagdaleneCarcass.
//=============================================================================
class MagdaleneCarcass extends Female1Carcass;

defaultproperties
{
	ItemName="Unconscious Magdalene"
	BindName="DeadBody"
	Mesh2=LodMesh'DeusExCharacters.GFM_TShirtPants_CarcassB'
	Mesh3=LodMesh'DeusExCharacters.GFM_TShirtPants_CarcassC'
	Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants_Carcass'
	MultiSkins(0)=Texture'CNN.MagdaleneFace1'
	MultiSkins(1)=Texture'CNN.MagdaleneFace1'
    MultiSkins(2)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(3)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(5)=Texture'CNN.MagdaleneFace1'
    MultiSkins(6)=Texture'DeusExCharacters.Skins.JCDentonTex3'
    MultiSkins(7)=Texture'DeusExCharacters.Skins.TiffanySavageTex1'
}