//=============================================================================
// GlassBottle.
//=============================================================================
Class GlassBottle expands DeusExDecoration;

defaultproperties
{
     HitPoints=10
     FragType=Class'DeusEx.GlassFragment'
     ItemName="GlassBottle"
     Mesh=LodMesh'CNN.GlassBottle'
     CollisionRadius=4.000000
     CollisionHeight=16.000000
     Mass=5.000000
     Buoyancy=10.000000
}
