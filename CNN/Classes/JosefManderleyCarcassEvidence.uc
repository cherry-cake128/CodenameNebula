class JosephManderleyCarcassEvidence extends BoxMedium;


defaultproperties
{
     HitPoints=10
     FragType=Class'DeusEx.PaperFragment'
     ItemName="Joseph Manderley carcass"
     bBlockSight=True
     CollisionRadius=42.000000
     CollisionHeight=5.000000
     Mass=50.000000
     Buoyancy=60.000000
     Mesh=LodMesh'DeusExCharacters.GM_Trench_CarcassC'
     MultiSkins(0)=Texture'DeusExCharacters.Skins.JosephManderleyTex0'
     MultiSkins(1)=Texture'DeusExCharacters.Skins.JosephManderleyTex2'
     MultiSkins(2)=Texture'DeusExCharacters.Skins.PantsTex5'
     MultiSkins(3)=Texture'DeusExCharacters.Skins.JosephManderleyTex0'
     MultiSkins(4)=Texture'DeusExCharacters.Skins.WaltonSimonsTex1'
     MultiSkins(5)=Texture'DeusExCharacters.Skins.JosephManderleyTex2'
     MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
}