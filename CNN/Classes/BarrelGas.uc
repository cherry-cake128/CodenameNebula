//=============================================================================
// BarrelGas.
//=============================================================================
class BarrelGas extends Containers;

defaultproperties
{
     bInvincible=True
     bFlammable=False
     ItemName="Gas Barrel"
     bBlockSight=True
     Mesh=LodMesh'CNN.BarrelGas'
     CollisionRadius=21.600000
     CollisionHeight=26.500000
     Mass=70.000000
     Buoyancy=80.000000
}