//=============================================================================
// CautionBox.
//=============================================================================
class CautionBox extends Containers;

defaultproperties
{
     HitPoints=10
     FragType=Class'DeusEx.PaperFragment'
     ItemName="CautionBox"
     bBlockSight=True
     Mesh=LodMesh'CNN.CautionBox'
     CollisionRadius=28.200000
     CollisionHeight=20.000000
     Mass=30.000000
     Buoyancy=60.000000
}
