//=============================================================================
// SinkMedium.
//=============================================================================
class SinkMedium extends Sinks;

defaultproperties
{
     Mesh=LodMesh'CNN.Sink2'
     CollisionRadius=32.000000
     CollisionHeight=3.500000
     Mass=30.000000
     Buoyancy=60.000000
}