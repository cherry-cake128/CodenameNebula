//=============================================================================
// Sinks.
//=============================================================================
class Sinks extends DeusExDecoration
	abstract;

defaultproperties
{
     bPushable=False
     bHighLight=False
     bInvincible=True
     bCanBeBase=True
     bBlockSight=True
     Physics=PHYS_None
     ItemName="Sink"
}