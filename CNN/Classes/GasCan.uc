//=============================================================================
// GasCan.
//=============================================================================
class GasCan extends Containers;

defaultproperties
{
     HitPoints=10
     bFlammable=False
     ItemName="Gas Can"
     Mesh=LodMesh'CNN.GasCan'
     DrawScale=0.500000
     CollisionRadius=9.000000
     CollisionHeight=12.250000
     Mass=20.000000
     Buoyancy=30.000000
}