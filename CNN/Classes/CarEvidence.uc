class CarEvidence extends BoxMedium;


defaultproperties
{
     HitPoints=10
     FragType=Class'CNN.AiMetalFragment'
     ItemName="The Dentons' car"
     bBlockSight=True
     CollisionRadius=118.000000
     CollisionHeight=49.580002
     Mass=50.000000
     Buoyancy=60.000000
     Mesh=LodMesh'DeusExDeco.CarWrecked'
}