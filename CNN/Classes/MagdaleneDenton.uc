class MagdaleneDenton extends Magdalene;

defaultproperties
{
	bInvincible=True
	bImportant=True
	bCanBleed=False
	bShowPain=False
	InitialAlliances(0)=(AllianceName=Player,AllianceLevel=1.000000,bPermanent=True)
	Alliance=Player
	Mesh=LodMesh'DeusExCharacters.GFM_Dress'
    MultiSkins(0)=Texture'DeusExCharacters.Skins.NicoletteDuClareTex0'
    MultiSkins(1)=Texture'DeusExCharacters.Skins.ScientistFemaleTex3'
    MultiSkins(2)=Texture'CNN.Skins.MagdaleneSkirt'
    MultiSkins(3)=Texture'CNN.MagdaleneDress'
    MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(5)=Texture'CNN.MagdaleneFace1'
    MultiSkins(6)=Texture'CNN.MagdaleneFace1'
    MultiSkins(7)=Texture'CNN.MagdaleneFace1'
	BindName="Magdalene"
	FamiliarName="Magdalene"
	UnfamiliarName="Magdalene"
}
