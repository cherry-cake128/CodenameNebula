class CNNTextures expands Object abstract;

// Character Textures

#exec TEXTURE IMPORT FILE=Textures\TantalusFace.pcx NAME=TantalusFace GROUP=Skins MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\TantalusBlack.pcx NAME=TantalusBlack GROUP=Skins MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\TantalusGinger.pcx NAME=TantalusGinger GROUP=Skins MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\TantalusGoatee.pcx NAME=TantalusGoatee GROUP=Skins MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\TantalusAsian.pcx NAME=TantalusAsian GROUP=Skins MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\NSFJacket.pcx NAME=NSFJacket GROUP=Skins MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\TantalusMenu1.pcx NAME=TantalusMenu1 GROUP=UI MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\TantalusMenu2.pcx NAME=TantalusMenu2 GROUP=UI MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\TantalusMenu3.pcx NAME=TantalusMenu3 GROUP=UI MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\TantalusMenu4.pcx NAME=TantalusMenu4 GROUP=UI MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\TantalusMenu5.pcx NAME=TantalusMenu5 GROUP=UI MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\skilliconFrench.pcx NAME=skilliconFrench GROUP=SkillIcons MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\skilliconPiloting.pcx NAME=skilliconPiloting GROUP=SkillIcons MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\mea1.pcx NAME=mea1 GROUP=DecoTex 
#exec TEXTURE IMPORT FILE=Textures\mea2.pcx NAME=mea2 GROUP=DecoTex 
#exec TEXTURE IMPORT FILE=Textures\mea3.pcx NAME=mea3 GROUP=DecoTex 
#exec TEXTURE IMPORT FILE=Textures\MagdaleneFace1.pcx NAME=MagdaleneFace1 GROUP=Skins MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\MagdaleneFace2.pcx NAME=MagdaleneFace2 GROUP=Skins MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\MagdaleneDress.pcx NAME=MagdaleneDress GROUP=Skins MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\MagdaleneSkirt.pcx NAME=MagdaleneSkirt GROUP=Skins MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\UberAllesHelmet.pcx NAME=UberAllesHelmet GROUP=Skins MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\paulsshirt.pcx NAME=paulsshirt GROUP=Skins MIPS=Off
#exec TEXTURE IMPORT FILE=Textures\apocalypseinside.pcx NAME=apocalypseinside
#exec TEXTURE IMPORT FILE=Textures\codenamenebula.pcx NAME=codenamenebula
#exec TEXTURE IMPORT FILE=Textures\codenamenebula_credits.pcx NAME=codenamenebula_credits
#exec TEXTURE IMPORT FILE=Textures\chester.bmp NAME=chester
#exec TEXTURE IMPORT FILE=Textures\chester_credits.bmp NAME=chester_credits
//from Nihilum
#exec TEXTURE IMPORT NAME=RusCopTex2 FILE=Textures\RusCopTex2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=RusCopTex0 FILE=Textures\RusCopTex0.pcx GROUP=Skins FLAGS=2
#exec TEXTURE IMPORT NAME=RusCopTex1 FILE=Textures\RusCopTex1.pcx GROUP=Skins

defaultproperties
{
}
