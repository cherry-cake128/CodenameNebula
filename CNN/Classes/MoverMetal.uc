//-----------------------------------------------------------
//
//-----------------------------------------------------------
class MoverMetal expands CNNMover;

DefaultProperties
{
	bDrawExplosion=True

	ExplodeSound1=Sound'DeusExSounds.Generic.WoodBreakLarge'
	ExplodeSound2=Sound'DeusExSounds.Robot.RobotExplode'

	FragmentClass=Class'DeusEx.MetalFragment'
	FragmentScale=1.000000
	FragmentTexture=Texture'Airfield.Metal.747_metal'

	NumFragments=8
}
