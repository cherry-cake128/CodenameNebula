//=============================================================================
// ClipBoard.
//=============================================================================
class ClipBoard extends InformationDevices;

defaultproperties
{
     bCanBeBase=True
     ItemName="ClipBoard"
     Mesh=LodMesh'CNN.ClipBoard'
     CollisionRadius=12.000000
     CollisionHeight=0.760000
     Mass=3.000000
     Buoyancy=5.000000
}
