//-----------------------------------------------------------
//  Test cop carcass to demonstate corpse burning effect
//-----------------------------------------------------------
class MJ12TroopFMCarcass extends TestFMCarcass;

DefaultProperties
{

Mesh=LodMesh'DeusExCharacters.GM_Jumpsuit_Carcass'
Mesh2=LodMesh'DeusExCharacters.GM_Jumpsuit_CarcassB'
Mesh3=LodMesh'DeusExCharacters.GM_Jumpsuit_CarcassC'
	MultiSkins(0)=Texture'DeusExCharacters.Skins.SkinTex1'
    MultiSkins(1)=Texture'DeusExCharacters.Skins.MJ12TroopTex1'
    MultiSkins(2)=Texture'DeusExCharacters.Skins.MJ12TroopTex2'
    MultiSkins(3)=Texture'DeusExCharacters.Skins.SkinTex1'
    MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(5)=Texture'DeusExCharacters.Skins.MJ12TroopTex3'
    MultiSkins(6)=Texture'DeusExCharacters.Skins.MJ12TroopTex4'
    MultiSkins(7)=Texture'DeusExItems.Skins.PinkMaskTex'

}

