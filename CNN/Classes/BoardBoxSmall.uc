//=============================================================================
// BoardBoxSmall.
//=============================================================================
class BoardBoxSmall extends Containers;

defaultproperties
{
     HitPoints=10
     FragType=Class'DeusEx.PaperFragment'
     ItemName="BoardBox"
     bBlockSight=True
     Mesh=LodMesh'CNN.BoardBoxSmall'
     CollisionRadius=14.500000
     CollisionHeight=8.500000
     Mass=15.000000
     Buoyancy=30.000000
}
