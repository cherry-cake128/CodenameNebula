class BobPageHolo extends BobPage;

defaultproperties
{
	Orders=Standing
    bShowPain=False
    bHateShot=False
    bHateInjury=False
    bReactPresence=False
    bReactAlarm=False
    bReactProjectiles=False
    Tag=BobPage
    Style=STY_Translucent
    bUnlit=True
    bCollideActors=False
    bBlockActors=False
    bBlockPlayers=False
}