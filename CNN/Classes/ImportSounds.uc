class ImportSounds expands Object
abstract;

#exec AUDIO IMPORT FILE="Sounds\dsdoropn.wav" name="DoorOpen1" GROUP="Stuff"
#exec AUDIO IMPORT FILE="Sounds\TantalusName.wav" name="TantalusName" GROUP="moon"
#exec AUDIO IMPORT FILE="Sounds\ChesterScream.wav" name="ChesterScream" GROUP="moon"

defaultproperties
{
}
