//We want it to look like this:

class ImportConversations expands Object
abstract;

// Import conversations
#exec CONVERSATION IMPORT FILE="Conversations\MoonCNN.Con"
#exec CONVERSATION IMPORT FILE="Conversations\OpheliaDocksAndL1.Con"
#exec CONVERSATION IMPORT FILE="Conversations\OpheliaL2.Con"
#exec CONVERSATION IMPORT FILE="Conversations\DL_Moon.Con"
#exec CONVERSATION IMPORT FILE="Conversations\OpheliaL2ArmMagdalene.Con"

// shared conversations

// Intro/Endgame

// AI Barks

// yeah

defaultproperties
{
}
