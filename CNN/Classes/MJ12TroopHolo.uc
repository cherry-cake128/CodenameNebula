class MJ12TroopHolo extends MJ12Troop;

defaultproperties
{
    Orders=Standing
    bShowPain=False
    bHateShot=False
    bHateInjury=False
    bReactPresence=False
    bReactAlarm=False
    bReactProjectiles=False
    Tag=MJ12TroopHolo
    BindName=MJ12TroopHolo
    Style=STY_Translucent
    bUnlit=True
    bCollideActors=False
    bBlockActors=False
    bBlockPlayers=False
}
