//=============================================================================
// gratewindow.
//=============================================================================
class gratewindow expands Decoration;

defaultproperties
{
     bStatic=False
     DrawType=DT_Mesh
     Mesh=LodMesh'CNN.GrateWindow'
     CollisionRadius=126.703125
     CollisionHeight=77.015625
}
