class BuddhaEvidence extends BoxMedium;


defaultproperties
{	
     HitPoints=10
     FragType=Class'CNN.AiMetalFragment'
     ItemName="Buddha from Ahnenerbe experition"
     bBlockSight=True
     CollisionRadius=42.000000
     CollisionHeight=5.000000
     Mass=50.000000
     Buoyancy=60.000000
     Mesh=LodMesh'DeusExDeco.HKBuddha'
}