//-----------------------------------------------------------
// just like WeaponDogBite but without sounds
//-----------------------------------------------------------
class CNNWeaponUPSMelee expands WeaponNPCMelee;

DefaultProperties
{
     ShotTime=0.500000
     HitDamage=15
     maxRange=80
     AccurateRange=80
     Misc1Sound=None;
     Misc2Sound=None;
     Misc3Sound=None;
}
