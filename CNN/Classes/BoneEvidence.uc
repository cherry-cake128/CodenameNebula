class BoneEvidence extends BoxSmall;


defaultproperties
{
     HitPoints=100
     FragType=Class'DeusEx.WoodFragment'
     ItemName="a Bone of a Desposyni"
     bBlockSight=True
     CollisionRadius=42.000000
     CollisionHeight=5.000000
     Mass=50.000000
     Buoyancy=60.000000
     Mesh=LodMesh'DeusExDeco.BoneFemur'
}