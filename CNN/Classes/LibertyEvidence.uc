class LibertyEvidence extends CrateUnbreakableLarge;


defaultproperties
{
     HitPoints=100
     FragType=Class'CNN.AiMetalFragment'
     ItemName="Statue of Liberty Crown"
     bBlockSight=True
     CollisionRadius=42.000000
     CollisionHeight=5.000000
     Mass=50.000000
     Buoyancy=60.000000
     Mesh=LodMesh'DeusExDeco.NYLibertyTop'
}