//-----------------------------------------------------------
// ApocalypseInsideMenuScreenNewGame
//-----------------------------------------------------------
class ApocalypseInsideMenuScreenNewGame expands MenuScreenNewGame;

DefaultProperties
{
    texPortraits(0)=Texture'CNN.TantalusMenu1'
    texPortraits(1)=Texture'CNN.TantalusMenu2'
    texPortraits(2)=Texture'CNN.TantalusMenu3'
    texPortraits(3)=Texture'CNN.TantalusMenu4'
    texPortraits(4)=Texture'CNN.TantalusMenu5'
}
