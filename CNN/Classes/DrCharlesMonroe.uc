class DrCharlesMonroe extends Doctor;

defaultproperties
{
    Orders=Standing
    bInvincible=True
    bHateShot=False
    bFearShot=False
    bFearIndirectInjury=False
    bFearCarcass=False
    bFearDistress=False
    bFearAlarm=False
    InitialInventory(0)=(Inventory=Class'DeusEx.WineBottle')
    Tag=DoctorMephistopheles
    Texture=None
    Mesh=LodMesh'DeusExCharacters.GM_Suit'
    MultiSkins(0)=Texture'DeusExCharacters.Skins.MIBTex0'
    MultiSkins(1)=Texture'DeusExCharacters.Skins.PantsTex10'
    MultiSkins(2)=None
    MultiSkins(3)=Texture'DeusExCharacters.Skins.ChefTex1'
    MultiSkins(4)=Texture'DeusExCharacters.Skins.ChefTex1'
    MultiSkins(5)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(6)=Texture'DeusExItems.Skins.BlackMaskTex'
    MultiSkins(7)=Texture'DeusExCharacters.Skins.PonytailTex1'
    BindName="DrMephistopheles"
    FamiliarName="Dr. Mephistopheles"
    UnfamiliarName="Dr. Charles Monroe"
}
