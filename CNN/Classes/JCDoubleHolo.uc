class JCDoubleHolo extends JCDouble;

defaultproperties
{
	MultiSkins(4)=Texture'CNN.Skins.paulsshirt'
	Orders=Standing
    bShowPain=False
    bHateShot=False
    bHateInjury=False
    bReactPresence=False
    bReactAlarm=False
    bReactProjectiles=False
    Tag=JCDouble
    bUnlit=True
    bCollideActors=False
    bBlockActors=False
    bBlockPlayers=False
}