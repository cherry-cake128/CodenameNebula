//=============================================================================
// AmmoBounceDisc. 	(c) 2003 JimBowen
//=============================================================================
class AmmoBounceDisc expands AmmoDisc;

//---END-CLASS---

defaultproperties
{
     AmmoAmount=50
     MaxAmmo=400
     ItemName="Polymer bounce discs"
     Description="Polymer based, sharpened discs that bounce off walls. They travel faster than explosive discs because they are lighter and more streamlined. They also do not fall out of the air like explosive discs due to an inbuilt areofoil shape to compenstate for gravity."
     beltDescription="POLY"
}
