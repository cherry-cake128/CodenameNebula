//=============================================================================
// FireSprite. 	(c) 2003 JimBowen
//=============================================================================
class FireSprite expands Effects;

//---END-CLASS---

defaultproperties
{
     Physics=PHYS_Projectile
     RemoteRole=ROLE_None
     LifeSpan=3.000000
     Velocity=(Z=10.000000)
     DrawType=DT_Sprite
     Style=STY_Masked
     Texture=FireTexture'Effects.Fire.flame_b'
}
