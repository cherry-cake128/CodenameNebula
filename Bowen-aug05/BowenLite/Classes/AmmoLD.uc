//=============================================================================
// AmmoLD.
//=============================================================================
class AmmoLD extends BowenAmmo;

//---END-CLASS---

defaultproperties
{
     AmmoAmount=4
     MaxAmmo=50
     PickupViewMesh=LodMesh'DeusExItems.TestBox'
     Icon=Texture'DeusExUI.Icons.BeltIconNanoVirusGrenade'
     beltDescription="LOCATOR"
     Mesh=LodMesh'DeusExItems.TestBox'
     CollisionRadius=22.500000
     CollisionHeight=16.000000
     bCollideActors=True
}