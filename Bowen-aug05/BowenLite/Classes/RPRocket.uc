//=============================================================================
// RPRocket.
//=============================================================================
class RPRocket expands Rocket;

//---END-CLASS---

defaultproperties
{
     mpBlastRadius=150.000000
     ItemName="Rocket Pod"
     speed=1200.000000
     Damage=45.000000
     LifeSpan=4.000000
}
