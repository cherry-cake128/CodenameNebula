//================================================
// FlareLight.
//================================================

class FlareLight expands BowenBasicActor2;

defaultproperties
{
	LightBrightness=50
        LightEffect=LT_NonIncidence
	LightType=LT_Steady
	LightRadius=85
        LightHue=0
	LightSaturation=255
	bHidden=True
	LifeSpan=120
	bAlwaysRelevant=True
}