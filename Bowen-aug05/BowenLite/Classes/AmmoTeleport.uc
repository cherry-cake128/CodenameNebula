//=============================================================================
// AmmoTeleport. 	(c) 2003 JimBowen
//=============================================================================
class AmmoTeleport expands BowenAmmo;

//---END-CLASS---

defaultproperties
{
     AmmoAmount=10
     MaxAmmo=50
     ItemName="Teleport gun ammo"
     Description="Power pack for the BowenCo teleport gun. This pack will be sufficient for an indefinate number of uses, since the teleport gun requires very little energy to run the primary function (line-of-sight teleport of user)"
     Skin=Texture'DeusExDeco.Skins.BarrelAmbrosiaTex1'
     Mesh=LodMesh'DeusExItems.Credits'
}
