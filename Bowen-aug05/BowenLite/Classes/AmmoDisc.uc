//=============================================================================
// AmmoDisc. 	(c) 2003 JimBowen
//=============================================================================
class AmmoDisc expands BowenAmmo;

//---END-CLASS---

defaultproperties
{
     AmmoAmount=24
     MaxAmmo=240
     ItemName="remote detonated discs"
     ItemArticle="some"
     PickupViewMesh=LodMesh'DeusExItems.GEPAmmo'
     Description="Remote detonated discs for the BowenCo Disc Launcher"
     beltDescription="REMOTE"
     Mesh=LodMesh'DeusExItems.GEPAmmo'
     CollisionRadius=18.000000
     CollisionHeight=7.800000
}
