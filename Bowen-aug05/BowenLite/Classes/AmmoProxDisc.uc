//=============================================================================
// AmmoProxDisc. 	(c) 2003 JimBowen
//=============================================================================
class AmmoProxDisc expands AmmoDisc;

//---END-CLASS---

defaultproperties
{
     AmmoAmount=12
     ItemName="Proximity discs"
     Description="Proximity mines for the BowenCo disc launcher"
     beltDescription="PROX"
}
