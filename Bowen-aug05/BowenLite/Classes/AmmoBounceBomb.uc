//=============================================================================
// AmmoBounceBomb.
//=============================================================================
class AmmoBounceBomb extends DeusExAmmo;

//---END-CLASS---

defaultproperties
{
     AmmoAmount=2
     MaxAmmo=20
     PickupViewMesh=LodMesh'DeusExItems.TestBox'
     Icon=Texture'DeusExUI.Icons.BeltIconWeaponNanoVirus'
     beltDescription="BOUNCE"
     Mesh=LodMesh'DeusExItems.TestBox'
     CollisionRadius=22.500000
     CollisionHeight=16.000000
     bCollideActors=True
}
