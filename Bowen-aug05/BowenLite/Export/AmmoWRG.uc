//================================================================================
// AmmoWRG.
//================================================================================
class AmmoWRG extends BowenAmmo;

defaultproperties
{
    AmmoAmount=1
    MaxAmmo=10
    PickupViewMesh=LodMesh'DeusExItems.TestBox'
    Icon=Texture'DeusExUI.Icons.BeltIconGasGrenade'
    beltDescription="DISARM"
    Mesh=LodMesh'DeusExItems.TestBox'
    CollisionRadius=22.50
    CollisionHeight=16.00
    bCollideActors=True
}