//================================================================================
// RPRocket.
//================================================================================
class RPRocket extends Rocket;

defaultproperties
{
    mpBlastRadius=150.00
    ItemName="Rocket Pod"
    speed=1200.00
    Damage=45.00
    LifeSpan=4.00
}