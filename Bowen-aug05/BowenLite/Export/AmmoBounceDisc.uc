//================================================================================
// AmmoBounceDisc.
//================================================================================
class AmmoBounceDisc extends AmmoDisc;

defaultproperties
{
    AmmoAmount=12
    ItemName="Polymer bounce discs"
    Description="Polymer based, sharpened discs that bounce off walls. They travel faster than explosive discs because they are lighter and more streamlined. They also do not fall out of the air like explosive discs due to an inbuilt areofoil shape to compenstate for gravity."
    beltDescription="POLY"
}