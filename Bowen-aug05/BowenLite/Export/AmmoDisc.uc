//================================================================================
// AmmoDisc.
//================================================================================
class AmmoDisc extends BowenAmmo;

defaultproperties
{
    AmmoAmount=24
    MaxAmmo=240
    ItemName="remote detonated discs"
    ItemArticle="some"
    PickupViewMesh=LodMesh'DeusExItems.GEPAmmo'
    Description="Remote detonated discs for the BowenCo Disc Launcher"
    beltDescription="REMOTE"
    Mesh=LodMesh'DeusExItems.GEPAmmo'
    CollisionRadius=18.00
    CollisionHeight=7.80
}