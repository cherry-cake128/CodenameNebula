//================================================================================
// AmmoProxDisc.
//================================================================================
class AmmoProxDisc extends AmmoDisc;

defaultproperties
{
    AmmoAmount=12
    ItemName="Proximity discs"
    Description="Proximity mines for the BowenCo disc launcher"
    beltDescription="PROX"
}