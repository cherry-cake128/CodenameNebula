//================================================================================
// FireSprite.
//================================================================================
class FireSprite extends Effects;

defaultproperties
{
    Physics=6
    RemoteRole=0
    LifeSpan=3.00
    Velocity=(X=0.00, Y=0.00, Z=10.00)
    DrawType=1
    Style=2
    Texture=FireTexture'Effects.Fire.flame_b'
}