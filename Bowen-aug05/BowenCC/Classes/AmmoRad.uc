//=============================================================================
// AmmoRad. 	(c) 2003 JimBowen
//=============================================================================
class AmmoRad expands BowenAmmo;

//---END-CLASS---

defaultproperties
{
     AmmoAmount=30
     MaxAmmo=150
     ItemName="Radiation gun ammo"
     Description="Power cell for the BowenCo Radiation Cannon"
     Skin=Texture'DeusExDeco.Skins.BarrelAmbrosiaTex1'
     Mesh=LodMesh'DeusExItems.Credits'
}
