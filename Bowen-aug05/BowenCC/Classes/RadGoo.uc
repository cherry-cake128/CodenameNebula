//=============================================================================
// RadGoo. 	(c) 2003 JimBowen
//=============================================================================
class RadGoo expands DeusExDecal;

//---END-CLASS---

defaultproperties
{
     MultiDecalLevel=2
     Style=STY_Translucent
     Texture=Texture'BowenCust.Effects.RadGoo'
     bUnlit=False
}
