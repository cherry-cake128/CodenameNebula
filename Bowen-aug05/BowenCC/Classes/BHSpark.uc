//=============================================================================
// BHSpark.
//=============================================================================
class BHSpark expands Tracer;

//---END-CLASS---

defaultproperties
{
     bEmitDanger=False
     MaxSpeed=10000.000000
     MomentumTransfer=100
     LifeSpan=3.000000
     AnimRate=10.000000
     DrawType=DT_Sprite
     Style=STY_Translucent
     Texture=Texture'BowenCust.Effects.BlueSpark'
     DrawScale=1.500000
     LightBrightness=128
     LightHue=80
     LightSaturation=240
     LightRadius=9
     LightPeriod=75
     LightPhase=100
     NetPriority=0.500000
     NetUpdateFrequency=20.000000
}
