//=============================================================================
// AmmoGuidedMissile. 	(c) 2003 JimBowen
//=============================================================================
class AmmoGuidedMissile expands AmmoMissile;

//---END-CLASS---

defaultproperties
{
     ItemName="laser guided missile"
     ItemArticle="a"
     Description="A variation on the LAW rocket, the BowenCo laser guided missile follows a laser spot to the target, so that last minute course corrections can be made manually by the user."
     beltDescription="LGM"
}
