//=============================================================================
// RadSplat. 	(c) 2003 JimBowen
//=============================================================================
class RadSplat expands DeusExDecal;

//---END-CLASS---

defaultproperties
{
     Style=STY_Translucent
     Texture=Texture'BowenCust.Effects.RadBlob'
     bUnlit=False
}
