//=============================================================================
// AmmoNIKITARocket. 	(c) 2003 JimBowen
//=============================================================================
class AmmoNIKITARocket expands AmmoMissile;

//---END-CLASS---

defaultproperties
{
     AmmoAmount=3
     ItemName="NIKITA electro-optical rockets"
     Description="An electro-optically guided rocket. The NIKITA is used by agenst seeking maximum precision. Designed for the BowenCo missile launcher. User-Interface for the BowenCo NIKITA rocket was kindly supplied by Morpheus."
     beltDescription="NIKITA"
}
