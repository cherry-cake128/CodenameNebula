<DC=255,255,255>
<P>Until I find a way to broadcast to your infolink I shall use public terminals as mediums to communicate with you from now on. These messages will appear to you only and MJ12 will not find out.
<P>The Oracle found favor with you, Tantalus Denton. While my powers are limited, I was able to detonate this Man In Black who was sent to assassinate you and the leader of Mole People with whom you were in contact.
<P>Quest given: follow the laser breadcrumbs. Use code 56535 to open the crime unit grate. There�s a black helicopter waiting for you where the lasers end. I sent it to take you to Paris. Don�t worry about having to navigate it or hiring a third-party pilot. This helicopter is unmanned and it has enough fuel for a round trip to Paris.
<P>Magdalene could not make it safe to the Moon base and had to join the allies of the resistance movement, the Knight Templar.
<P>Once in Paris I will give you a new identity and a UNATCO security clearance Angel 0/A. Magdalene is protected by a small group of Knight Templar who are witchhunted by both UNATCO and Interpol for their resistance to MJ12. 
<P>I will help you because I am curious about your feelings. I�d like to know what it the emotion you call �love�.
