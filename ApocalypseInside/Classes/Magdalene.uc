class Magdalene extends Female1;


defaultproperties
{
	bInvincible=True
	bImportant=True
     bCanBleed=False
     bShowPain=False
     InitialAlliances(0)=(AllianceName=Player,AllianceLevel=1.000000,bPermanent=True)
     Alliance=Player
    MultiSkins(0)=Texture'ApocalypseInside.MagdaleneFace1'
    MultiSkins(1)=Texture'ApocalypseInside.MagdaleneFace1'
    MultiSkins(2)=Texture'ApocalypseInside.MagdaleneFace1'
    MultiSkins(3)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(4)=FireTexture'Effects.Laser.LaserSpot2'
    MultiSkins(5)=Texture'ApocalypseInside.MagdaleneFace1'
    MultiSkins(6)=Texture'DeusExCharacters.Skins.TiffanySavageTex2'
    MultiSkins(7)=Texture'DeusExCharacters.Skins.TiffanySavageTex1'
     BindName="Magdalene"
     FamiliarName="Magdalene"
     UnfamiliarName="Magdalene"
}
