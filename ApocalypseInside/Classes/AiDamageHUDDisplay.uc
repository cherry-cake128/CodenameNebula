//=============================================================================
// AiDamageHUDDisplay
//=============================================================================
class AiDamageHUDDisplay extends DamageHUDDisplay
	transient;

defaultproperties
{
     IconWidth=64
     IconHeight=64
     iconMargin=2
     arrowiconWidth=128
     arrowiconHeight=128
     fadeTime=2.000000
     msgAbsorbed="%d%% Absorb"
}
