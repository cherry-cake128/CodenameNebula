//=============================================================================
// Pallet.
//=============================================================================
class Pallet extends AIDeco;

defaultproperties
{
     bCanBeBase=True
     bFlammable=True
     ItemName="Pallet"
     Mesh=LodMesh'ApocalypseInside.Pallet'
     FragType=Class'DeusEx.WoodFragment'
     SoundRadius=16
     CollisionRadius=24.000000
     CollisionHeight=3.780001
     Mass=10.000000
     Buoyancy=8.000000
}