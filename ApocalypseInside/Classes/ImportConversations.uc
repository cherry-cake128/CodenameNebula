//We want it to look like this:

class ImportConversations expands Object
abstract;

// Import conversations
#exec CONVERSATION IMPORT FILE="Conversations\Prologue.Con"
#exec CONVERSATION IMPORT FILE="Conversations\Chapter01.Con"
#exec CONVERSATION IMPORT FILE="Conversations\Chapter02.Con"
#exec CONVERSATION IMPORT FILE="Conversations\Chapter03.Con"
#exec CONVERSATION IMPORT FILE="Conversations\Chapter04.Con"
#exec CONVERSATION IMPORT FILE="Conversations\Chapter05.Con"
#exec CONVERSATION IMPORT FILE="Conversations\Chapter06.Con"
#exec CONVERSATION IMPORT FILE="Conversations\DL_Chapter03.Con"
#exec CONVERSATION IMPORT FILE="Conversations\DL_Chapter04.Con"
#exec CONVERSATION IMPORT FILE="Conversations\DL_Chapter05.Con"
#exec CONVERSATION IMPORT FILE="Conversations\DL_Chapter06.Con"
#exec CONVERSATION IMPORT FILE="Conversations\Mission96.Con"


// shared conversations

// Intro/Endgame

// AI Barks

// yeah

defaultproperties
{
}
