//================================================================================
// AiSkillChinese.
//================================================================================

class AiSkillChinese extends Skill;


defaultproperties
{
    mpCost1=1000

    mpCost2=1000

    mpCost3=1000

    mpLevel0=1.00

    mpLevel1=1.00

    mpLevel2=2.00

    mpLevel3=3.00

    SkillName="Chinese"

    Description="Learn Mandarin to communicate with the Chinese people in their language. Understand what they are talking about when they think you are an ignorant 'gwailo', convince them for better deals and read Chinese text on computer terminals.|n|nUNTRAINED: An agent cannot speak Mandarin.|n|nTRAINED: Basic knowledge that is enough to  understand spoken foreign language.|n|nADVANCED: An agent can effectively express him/herself while speaking a foreign language.|n|nMASTER: An agent can read Chinese characters and comprehend the written text."

    SkillIcon=Texture'DeusExUI.UserInterface.ComputerLogonLogoPRChina'

    cost(0)=900

    cost(1)=1800

    cost(2)=3000

    LevelValues(0)=1.00

    LevelValues(1)=2.00

    LevelValues(2)=2.50

    LevelValues(3)=3.00

}