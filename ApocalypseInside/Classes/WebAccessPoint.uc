class WebAccessPoint extends ComputerPublic;

defaultproperties
{
     terminalType=Class'DeusEx.NetworkTerminalPublic'
     ItemName="Web 3.0 Access Point"
     Physics=PHYS_None
	 MultiSkins(0)=Texture'ApocalypseInside.mea1'
     Mesh=LodMesh'DeusExDeco.ComputerPublic'
     ScaleGlow=2.000000
     CollisionHeight=49.139999
     BindName="ComputerPublic"
}
