//-----------------------------------------------------------
//
//-----------------------------------------------------------
class AiAmmoDartPoisonZyme extends AmmoDart;

var string AmmoTypeName;

DefaultProperties
{
	AmmoTypeName="Zyme Darts"
    ItemName="Zyme Darts"
    Icon=Texture'DeusExUI.Icons.BeltIconAmmoDartsPoison'
    largeIcon=Texture'DeusExUI.Icons.LargeIconAmmoDartsPoison'
    Description="A mini-crossbow dart tipped with a succinylcholine-variant that causes panic."
    beltDescription="ZYME DART"
    Skin=Texture'DeusExItems.Skins.AmmoDartTex3'
}