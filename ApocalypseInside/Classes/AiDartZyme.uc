//-----------------------------------------------------------
//
//-----------------------------------------------------------
class AiDartZyme extends DartPoison;

DefaultProperties
{
	mpDamage=10.000000
	DamageType=Zymed
	spawnAmmoClass=Class'DeusEx.AmmoDartPoison'
	ItemName="Zyme Tranquilizer Dart"
	Damage=5.000000
}