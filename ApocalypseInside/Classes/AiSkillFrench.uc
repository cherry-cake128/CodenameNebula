//================================================================================
// AiSkillFrench.
//================================================================================

class AiSkillFrench extends Skill;


defaultproperties
{
    mpCost1=1000

    mpCost2=1000

    mpCost3=1000

    mpLevel0=1.00

    mpLevel1=1.00

    mpLevel2=2.00

    mpLevel3=3.00

    SkillName="French"

    Description="Earn trust of the French by speaking in their mother tong.|n|nUNTRAINED: An agent cannot speak French.|n|nTRAINED: Elementary French, survival level.|n|nADVANCED: Intermediate linguistic level, you can communicate with the French to convince them, especially patriots who are rather annoyed by non-Francophone tourists.|n|nMASTER: Vous pouvez liser ce text et les autres textes dans le monde."

    SkillIcon=Texture'ApocalypseInside.SkillIcons.skilliconFrench'

    cost(0)=900

    cost(1)=1800

    cost(2)=3000

    LevelValues(0)=1.00

    LevelValues(1)=2.00

    LevelValues(2)=2.50

    LevelValues(3)=3.00

}